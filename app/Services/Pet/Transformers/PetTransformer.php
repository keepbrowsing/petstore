<?php
namespace App\Services\Pet\Transformers;

use App\Services\Pet\Models\Pet;
use Illuminate\Database\Eloquent\Collection;

class PetTransformer
{
    /**
     * @param Pet $model
     * @return array
     */
    public function transformModel(Pet $model) : array
    {
        return [
            'id' => $model->id,
            'name' => $model->name,
            'tags' => $model->tags
                ? $model->tags->map(function ($tag) {
                    return [
                        'id' => $tag->tag->id,
                        'name' => $tag->tag->name,
                    ];
                })
                : [],
            'category' => $model->categories
                ? $model->categories->map(function ($category) {
                    return [
                        'id' => $category->category->id,
                        'name' => $category->category->name,
                        ];
                })
                : [],
            'images' => $model->images
                ? $model->images->map(function ($image) {
                    return $image->url;
                })
                : [],
        ];
    }

    /**
     * @param Collection $pets
     * @return mixed
     */
    public function transfromCollection(Collection $pets)
    {
        return $pets->map(function ($pet) {
            return $this->transformModel($pet);
        });
    }
}
