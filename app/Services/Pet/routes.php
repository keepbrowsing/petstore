<?php

use Illuminate\Contracts\Routing\Registrar;

/** @var Registrar $router */

// Pet routes
$router->group(['prefix' => 'v1.0/services'], function (Registrar $router) {
    $router->group(['prefix' => '/pet'], function (Registrar $router) {
        $router->get('/', 'PetController@index')->name('pet.index');
        $router->get('/{petId}', 'PetController@show')->name('pet.show');
        $router->get('/find-by-status/{status}', 'PetController@findPetByStatus')->name('pet.find_by_status');
        $router->post('/', 'PetController@store')->name('pet.store');
        $router->put('/{pet}', 'PetController@store')->name('pet.update');
        $router->delete('/{petId}', 'PetController@destroy')->name('pet.delete');
    });
});
