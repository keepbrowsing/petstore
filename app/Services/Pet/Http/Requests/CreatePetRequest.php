<?php

namespace App\Services\Pet\Http\Requests;

use App\Services\Pet\Models\Pet;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class CreatePetRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'status' => ['required', Rule::in(Pet::$supportedStatus), 'bail'],
            'name' => ['required', 'unique:pets,name'],
            'tags' => ['required'],
            'tags.*' => ['exists:tags,id'],
            'category' => ['required'],
            'category.*' => ['exists:categories,id'],
            'photoUrls' => 'array',
        ];
    }

    /**
     * @param Validator $validator
     */
    public function failedValidation(Validator $validator)
    {
        $errors =   (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json(
                [
                    'errors' => $errors
                ],
                JsonResponse::HTTP_UNPROCESSABLE_ENTITY
            )
        );
    }
}
