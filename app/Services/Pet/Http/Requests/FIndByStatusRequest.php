<?php

namespace App\Services\Pet\Http\Requests;

use App\Services\Pet\Models\Pet;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class FIndByStatusRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'status' => ['required', Rule::in(Pet::$supportedStatus)],
        ];
    }

    /**
     * @param Validator $validator
     */
    public function failedValidation(Validator $validator)
    {
        $errors =   (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json(
                [
                    'errors' => $errors
                ],
                JsonResponse::HTTP_UNPROCESSABLE_ENTITY
            )
        );
    }
}
