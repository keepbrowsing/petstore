<?php

namespace App\Services\Pet\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\Pet\Http\Requests\CreatePetRequest;
use App\Services\Pet\Transformers\PetTransformer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Services\Pet\Models\Pet;
use Illuminate\Support\Facades\DB;

class PetController extends Controller
{


    /**
     * @var PetTransformer
     */
    private $petTransformer;

    public function __construct(PetTransformer $petTransformer)
    {
        $this->petTransformer = $petTransformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // Fetch all pets and return the json
        return response()->json(
            $this->petTransformer->transfromCollection(Pet::all())
        );
    }

    public function findPetByStatus(string $status)
    {
        if (in_array($status, Pet::$supportedStatus)) {
            $pets = Pet::where('status', '=', $status)->get();
            return response()->json(
                $this->petTransformer->transfromCollection($pets)
            );
        } else {
            // return invalid status
            return response()->json(['message' => 'Invalid Pet Status'], JsonResponse::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Store a newly created Pet.
     *
     * @param CreatePetRequest $request
     * @return Response
     */
    public function store(CreatePetRequest $request)
    {
        // All the validation is handled in CreatePetRequest
        DB::beginTransaction();

        try {
            $pet = Pet::create([
                'name' => $request->input('name'),
                'status' => $request->input('status'),
            ]);
            //save tags, we expect that the tags array is either null or the array of ids of tags from database.
            if ($tags = $request->input('tags')) {
                foreach ($tags as $tag) {
                    $pet->tags()->create([
                        'tag_id' => $tag
                    ]);
                }
            }

            //save categories, we expect that the tags array is either null or the array of ids of categories from database.
            if ($categories = $request->input('category')) {
                foreach ($categories as $category) {
                    $pet->categories()->create([
                        'category_id' => $category
                    ]);
                }
            }

            //save images
            if ($photoUrls = $request->input('photoUrls')) {
                foreach ($photoUrls as $photoUrl) {
                    $pet->images()->create([
                        'url' => $photoUrl
                    ]);
                }
            }
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Server Error'], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
        // If everything is fine with earlier DB transactions, then commit all transactions.
        DB::commit();

        return response()->json(
            $this->petTransformer->transformModel($pet)
        );
    }

    /**
     * Display the specified Pet.
     *
     * @param int $petId
     * @return JsonResponse
     */
    public function show(int $petId)
    {
        $pet = Pet::find($petId);
        if (!$pet) {
            return response()->json(['message' => 'Not Found!'], JsonResponse::HTTP_NOT_FOUND);
        } else {
            return response()->json(
                $this->petTransformer->transformModel($pet)
            );
        }
    }

    /**
     * Update the specified Pet in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified Pet from storage.
     *
     * @param int $petId
     * @return Response
     */
    public function destroy(int $petId)
    {
        $pet = Pet::find($petId);
        if (!$pet) {
            return response()->json(['message' => 'Not Found!'], JsonResponse::HTTP_NOT_FOUND);
        } else {
            $pet->delete();
            return response()->json(['message' => 'Successfully Deleted'], JsonResponse::HTTP_OK);
        }
    }
}
