<?php

namespace App\Services\Pet\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class PetCategories extends Model
{
    public $table = 'pet_categories';
    public $timestamps = false;

    protected $fillable = ['category_id'];

    /**
     * @return BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }
}
