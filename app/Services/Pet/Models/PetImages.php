<?php

namespace App\Services\Pet\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class PetImages extends Model
{
    public $table = 'pet_images';

    protected $fillable = ['url'];

    public $timestamps = false;

    /**
     * @return BelongsTo
     */
    public function pet()
    {
        return $this->belongsTo(Pet::class, 'pet_id');
    }
}
