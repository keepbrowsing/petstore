<?php

namespace App\Services\Pet\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class PetTags extends Model
{
    public $table = 'pet_tags';
    public $timestamps = false;

    protected $fillable = ['tag_id'];

    /**
     * @return BelongsTo
     */
    public function pet()
    {
        return $this->belongsTo(Pet::class, 'pet_id');
    }

    /**
     * @return BelongsTo
     */
    public function tag()
    {
        return $this->belongsTo(Tag::class, 'tag_id');
    }
}
