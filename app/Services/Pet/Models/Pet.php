<?php

namespace App\Services\Pet\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Pet extends Model
{
    public $table = 'pets';

    const STATUS_AVAILABLE = 'available';
    const STATUS_PENDING = 'pending';
    const STATUS_SOLD = 'sold';
    public static $supportedStatus = [self::STATUS_AVAILABLE, self::STATUS_PENDING, self::STATUS_SOLD];

    protected $fillable = ['name'];

    /**
     * @return HasMany
     */
    public function categories()
    {
        //return $this->hasMany(Category::class, 'pet_categories', 'pet_id', 'category_id');
        return $this->hasMany(PetCategories::class, 'pet_id');
    }

    /**
     * @return HasMany
     */
    public function tags()
    {
        //return $this->hasMany(Tag::class, 'pet_tags', 'pet_id', 'tag_id');
        return $this->hasMany(PetTags::class, 'pet_id');
    }

    /**
     * @return HasMany
     */
    public function images()
    {
        return $this->hasMany(PetImages::class, 'pet_id');
    }
}
