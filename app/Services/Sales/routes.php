<?php

use Illuminate\Contracts\Routing\Registrar;

/** @var Registrar $router */

// Order routes
$router->group(['prefix' => 'v1.0/services/order'], function (Registrar $router) {
    $router->get('/', SalesController::class, 'index')->name('order.index');
    $router->get('/{order}', SalesController::class, 'show')->name('order.show');
    $router->post('/', SalesController::class, 'store')->name('sales.store');
    $router->delete('/{order}', SalesController::class, 'destroy')->name('order.destroy');
});
