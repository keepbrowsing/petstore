<?php

namespace Tests\Feature;

use App\Services\Pet\Models\Pet;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\Services\Pet\Models\Category;
use App\Services\Pet\Models\Tag;
use Illuminate\Http\JsonResponse;
use Tests\TestCase;

class PetControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A test to check invalid request.
     */
    public function testInvalidPetCreationRequestIsHandled()
    {
        $request = [
            'name' => '',
            'status' => 'invalidStatus',
            'category' => [],
            'tags' => [],
            'photoUrls' => []
        ];

        $response = $this->post(route('pet.store', $request));
        $response->assertStatus(JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testGetPetDetailsFromValidId()
    {
        $pet = factory(Pet::class)->create();
        $response = $this->get(route('pet.show', [
            'petId' =>  $pet->id
        ]));
        $response->assertStatus(JsonResponse::HTTP_OK);
    }

    public function testInvalidPetIdThrowsError()
    {
        $response = $this->get(route('pet.show', [
            'petId' => 4444
        ]));
        $response->assertStatus(JsonResponse::HTTP_NOT_FOUND);
    }

    public function todoTestPetIsDeleted()
    {
        $pet = factory(Pet::class)->create();
        $response = $this->delete(route('pet.delete', [
            'petId' =>  $pet->id
        ]));
        $response->assertStatus(JsonResponse::HTTP_OK);
        $this->assertDatabaseMissing('pets', [
            'id' => $pet->id,
        ]);
    }

    public function testInvalidStatusIsHandled()
    {
        $response = $this->get(route('pet.find_by_status', [
            'status' => 'invalidStatus'
        ]));
        $response->assertStatus(JsonResponse::HTTP_BAD_REQUEST)
            ->assertJson(['message' => 'Invalid Pet Status' ]);
    }

    /**
     * A test to make sure pet is created with valid request.
     *
     * @return void
     */
    public function testCanCreatePet()
    {
        $category = factory(Category::class)->create();

        $tag = factory(Tag::class)->create();

        $request = [
            'name' => 'valid pet name',
            'status' => 'available',
            'category' => [$category->id],
            'tags' => [$tag->id],
            'photoUrls' => ['/myurl']
        ];
        $response = $this->post(route('pet.store', $request));
        $response->assertStatus(JsonResponse::HTTP_OK);
        $this->assertDatabaseHas('pets', [
            'name' => $request['name'],
        ]);
    }
}
