# Jumbo Technical Test - Laravel PHP Framework

This is a micro-service which acts as an API endpoints for Pet Manager

## Brief Overview
This is a simple API application which is supposed to manage pets related api requests for the Pet store.

## How to install and run the project?
```
composer install
```
### Setup
- When you are done with installation, copy the `.env.example` file to `.env`

  ```$ cp .env.example .env```


- Generate the application key

  ```$ php artisan key:generate```


- Add your database credentials to the necessary `env` fields

- Migrate the application

  ```$ php artisan migrate```

- Seed Database ( generate data for categories and tags)

  ```$ php artisan db:seed```

- Run the Tests

  ```$ php artisan test```
